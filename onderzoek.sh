#!/bin/bash

echo "The number of tweets send on January first 2016 at 12 o'clock:"
#Opening tweets from chosen date and hour | Every tweet on a new line using tweet2tab | Counting the lines.
zless /net/corpora/twitter2/Tweets/2016/01/20160101\:12.out.gz  | /net/corpora/twitter2/tools/tweet2tab text | wc -l

echo "The number tweets without retweets and tweets containing links:"
#Removing retweets from the results | Removing links from the results | Counting the lines.
zless /net/corpora/twitter2/Tweets/2016/01/20160101\:12.out.gz  | /net/corpora/twitter2/tools/tweet2tab text | grep -v 'RT' | grep -v 'https://' | wc -l

echo "The number of tweets of which get-gender.py could determine the gender:"
#Placing gender before tweet using get_gender.py | counting the lines.
zless /net/corpora/twitter2/Tweets/2016/01/20160101\:12.out.gz  | /net/corpora/twitter2/tools/tweet2tab text | grep -v 'RT' | grep -v 'https://' | python get-gender.py | wc -l

echo "The number of pre-processed tweets written by men:"
#Showing only tweets from male users | Counting the lines.
mtweets="$(zless /net/corpora/twitter2/Tweets/2016/01/20160101\:12.out.gz  | /net/corpora/twitter2/tools/tweet2tab text | grep -v 'RT' | grep -v 'https://' | python get-gender.py | grep '^male' | wc -l)"
echo $mtweets

echo "The number of pre-processed tweets written by women:"
#Showing only tweets from female users | Counting the lines.
ftweets="$(zless /net/corpora/twitter2/Tweets/2016/01/20160101\:12.out.gz  | /net/corpora/twitter2/tools/tweet2tab text | grep -v 'RT' | grep -v 'https://' | python get-gender.py | grep '^female' | wc -l)"
echo $ftweets

echo "The average amount of words used in tweets written by men:"
#Counting the total number of words written by men.
mwords="$(zless /net/corpora/twitter2/Tweets/2016/01/20160101\:12.out.gz  | /net/corpora/twitter2/tools/tweet2tab text | grep -v 'RT' | grep -v 'https://' | python get-gender.py | grep '^male' | wc -w)"
#Deviding the number of words by the number of tweets from male users.
echo "scale=2; $mwords/$mtweets" | bc -l

echo "The average amount of words used in tweets written by women:"
#Counting the total number of words written by women.
fwords="$(zless /net/corpora/twitter2/Tweets/2016/01/20160101\:12.out.gz  | /net/corpora/twitter2/tools/tweet2tab text | grep -v 'RT' | grep -v 'https://' | python get-gender.py | grep '^female' | wc -w)"
#Deviding the number of words by the number of tweets from female users.
echo "scale=2; $fwords/$ftweets" | bc -l
