#!/bin/bash
#This shell script analyzes the tweets of the twitter file from 1 March 2017 12:00.
#Open the twitter file from 1 March 2017 12:00; show the tweets, each on one line; count the lines.
echo "Number of tweets in the sample:"
zless "/net/corpora/twitter2/Tweets/2017/03/20170301:12.out.gz"  | /net/corpora/twitter2/tools/tweet2tab -i text | wc -l
#Sort the tweets from the file; show only unique tweets; count the lines.
echo "Number of unique tweets in the sample:"
zless "/net/corpora/twitter2/Tweets/2017/03/20170301:12.out.gz"  | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | wc -l

#Search for tweets (out of the unique tweets) that start with 'RT'; count the lines.
echo "Number of retweets in the sample (out of the unique tweets):"
zless "/net/corpora/twitter2/Tweets/2017/03/20170301:12.out.gz"  | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep '^RT' | wc -l

#Show the tweets (out of the unique tweets) that don't start with 'RT'; show the first 20 tweets.
echo "The first 20 unique tweets in the sample that are not retweets:"
zless "/net/corpora/twitter2/Tweets/2017/03/20170301:12.out.gz"  | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -v '^RT' | head -20
